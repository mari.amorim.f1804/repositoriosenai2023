const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,

            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,

            //Arrays (vetores) para armazenamento dos nomes de unuários e senhas
            usuarios: ["admin", "mari", "otavio"],
            senhas: ["123", "123", "maltinha123"],
            userAdmin: false,
            mostrarEntrada: false,

            //Variáveis para tratamento das informações dos novos usuários
            newUsername: "",
            newPassword: "",
            confirmPassword: "",

            mostrarLista: false,
        };//Fechamento return
    },//Fechamento data

    methods:{
        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                if(this.operador == "="){
                    this.valorDisplay = "";
                    this.operador = null;
                }
                // this.valorDisplay = this.valorDisplay + numero.toString();

                //adição simplificada
                this.valorDisplay += numero.toString();
            }
        },//Fechamento getNumero

        limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
        },//Fechamento limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){
                this.valorDisplay += ".";
            }//Fechamento if
        },//Fechamento decimal

        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                            break;
                        
                        case "-":
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;
                        case "*":
                            this.valorDisplay = (this.numeroAtual * displayAtual).toString();
                            break;
                        case "/":
                            this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                            break;

                    }//Fim do switch
                    this.numeroAnterior = this.numeroAtual;
                    this.numeroAtual = null;

                    if(this.operador != "0"){
                        this.operador = null;
                    }
                    //acertar o numero de casas decimais quando o resultado for decimal
                }//Fim do if
                else{
                    this.numeroAnterior = displayAtual;
                }//fim do else
            }//Fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
        },//Fim operações
    
        login(){
            //simulando uma requisisão de login assincrona 
            setTimeout(() => {
                if((this.usuario === "mari" && this.senha === "123") || 
                (this.usuario === "otavio" && this.senha === "maltinha123") ||
                (this.usuario === "adriano" && this.senha === "adri123") ||
                (this.usuario === "julia" && this.senha === "ju123")){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!💕";
                    window.location.href='index.calculadora.html';
                    // alert("Login efetuado com sucesso!");
                }//fechamento do if
                else{
                    //alert ("usuario ou senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!💔";
                    this.sucesso = null;}
            }, 1000);
        },//fechamento login

        login2(){
            this.mostrarEntrada = false;
            setTimeout(() => {
                this.mostrarEntrada = true;
                //verificação de usuário e senha cadastrado ou não nos arrays
                const index = this.usuarios.indexOf(this.usuario);
                if(index !== -1 && this.senhas[index] === this.senha){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!💕";

                    //Registrando o usuário no LocalStorage para lembrete de acessos
                    localStorage.setItem("usuario" , this.usuario);
                    localStorage.setItem("senha" , this.senha);

                    //verificando se o usuário é admin
                    if(this.usuario === "admin" && index === 0){
                        this.userAdmin = true;
                        this.sucesso = "logado como ADMIN! 💖";}
                }//fechamento if
                else{this.sucesso = null;
                    this.erro = "Usuário e/ou senha incorretos!💔";}
            }, 1000);
        },//fechamento login2

        paginaCadastro(){
            this.mostrarEntrada=false;
            if(this.userAdmin == true){
                this.erro=null;
                this.sucesso="Carregando Página de Cadastro...💞";
                this.mostrarEntrada=true;

                //Espera estratégica antes do carregamento da página
                setTimeout(() => {}, 2000);

                setTimeout(() => {
                    window.location.href='index.cadastro.html';
                }, 1000);
            }//fechamento if
            else{
                this.sucesso=null;
                setTimeout(() => {
                   this.mostrarEntrada=true;
                   this.erro="Sem privilégios de ADMIN! 💔";
                }, 1000);
            }//fechamento else
        },//fechamento página cadastro

        adicionarUsuario(){
            this.mostrarEntrada=false;
            this.usuario=localStorage.getItem("usuario");
            this.senha=localStorage.getItem("senha");

            setTimeout(() => {
                this.mostrarEntrada=true;
                
                if(this.usuario==="admin"){
                    /*verificando se o novo usuário é diferente de vazio e se ele já não está cadastrado no sistema*/
                    if(!this.usuarios.includes(this.newUsername) && this.newUsername !== "" && 
                    !this.newUsername.includes(" ")){
                        //validação da senha digitada para cadastro no array
                        if(this.newPassword !== "" && !this.newPassword.includes(" ") 
                        && this.newPassword === this.confirmPassword){
                            //inserindo o novo usuário e senha nos arrays
                            this.usuarios.push(this.newUsername);
                            this.senhas.push(this.newPassword);

                            //atualizando o usuário recém cadastrado no LocalStorage
                            localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                            localStorage.setItem("senhas", JSON.stringify(this.senhas));


                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";

                            this.erro = null;
                            this.sucesso = "Usuário cadastrado com sucesso!💘";
                        }//fechamento if password
                        else{
                            this.sucesso = null;
                            this.erro = "Por favor, informe uma senha válida!💔";

                            this.newUsername = "";
                            this.newPassword = "";
                            this.confirmPassword = "";
                        }
                    }//fechamento if includes

                    else{
                        this.erro = "Usuário já cadastrado ou inválido! Por favor digite um usuário diferente!💔";
                        this.sucesso = null;

                        this.newUsername = "";
                        this.newPassword = "";
                        this.confirmPassword = "";
                    }//fechamento else
                }//fechamento if admin

                else{
                    this.erro = "Não está logado como ADMIN!💔";
                    this.sucesso = null;

                    this.newUsername = "";
                    this.newPassword = "";
                    this.confirmPassword = "";
                }//fehamento else admin
            }, 300);
        },//fechamento adicionar usuário

        listarUsuarios(){
            if(localStorage.getItem("usuarios") && localStorage.getItem("senhas")){
                this.usuarios = JSON.parse(localStorage.getItem("usuarios"));
                this.senha = JSON.parse(localStorage.getItem("senhas"));
            }//fechamento if listar usuários

            this.mostrarLista = !this.mostrarLista;
        },//fechamento listar usuários
        excluirUsuario(){
            this.mostrarEntrada = false;
            if(usuario === "admin"){
                setTimeout(() => {
                    this.mostrarEntrada = true;
                    this.sucesso = null;
                    this.erro = "O usuário ADMIN não pode ser excluído!💔";
                }, 300);
                return;//força a saída deste bloco
            }//fechamento if
            if(confirm("Tem certeza que deseja excluir o usuário?")){
                const index = this.usuarios.indexOf(usuario);
                if(index !== -1){
                    this.usuarios.splice(index, 1);
                    this.senhas.splice(index, 1);

                    //atualiza o array usuários no localStorage
                    localStorage.setItem("usuarios", JSON.stringify(this.usuarios));
                    localStorage.setItem("senhas", JSON.stringify(this.senhas));

                    setTimeout(() => {
                        this.mostrarEntrada = true;
                        this.erro = null;
                        this.sucesso = "Usuário excluído com sucesso!💓";
                    }, 300);
                }//fechamento if index
            }//fechamento if
        },//fechamento excluir usuário
    },//Fechamento methods
}).mount("#app");//Fechamento app