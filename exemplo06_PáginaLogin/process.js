const {createApp} = Vue;

createApp({
    data(){
        return{
            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,
        }//fechamento return
    },//fechamento data
    methods:{
        login(){
            //simulando uma requisisão de login assincrona 
            setTimeout(() => {
                if((this.usuario === "mari" && this.senha === "123") || 
                (this.usuario === "otavio" && this.senha === "maltinha123")){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!💕";
                    // alert("Login efetuado com sucesso!");
                }//fechamento do if
                else{
                    //alert ("usuario ou senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!💔";
                    this.sucesso = null;
                }
            }, 1000);
        },//fechamento login
    },//fechamento methods

}).mount("#app");//fechamento app