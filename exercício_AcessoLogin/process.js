const {createApp} = Vue;
createApp({
    data(){
        return{
            valorDisplay:"0",
            operador:null,
            numeroAtual:null,
            numeroAnterior:null,

            usuario: '',
            senha: '',
            erro: null,
            sucesso: null,
        };//Fechamento return
    },//Fechamento data

    methods:{
        getNumero(numero){
            if(this.valorDisplay == "0"){
                this.valorDisplay = numero.toString();
            }
            else{
                if(this.operador == "="){
                    this.valorDisplay = "";
                    this.operador = null;
                }
                // this.valorDisplay = this.valorDisplay + numero.toString();

                //adição simplificada
                this.valorDisplay += numero.toString();
            }
        },//Fechamento getNumero

        limpar(){
            this.valorDisplay = "0";
            this.operador = null;
            this.numeroAnterior = null;
            this.numeroAtual = null;
        },//Fechamento limpar

        decimal(){
            if(!this.valorDisplay.includes(".")){
                this.valorDisplay += ".";
            }//Fechamento if
        },//Fechamento decimal

        operacoes(operacao){
            if(this.numeroAtual != null){
                const displayAtual = parseFloat(this.valorDisplay);
                if(this.operador != null){
                    switch(this.operador){
                        case "+":
                            this.valorDisplay = (this.numeroAtual + displayAtual).toString();
                            break;
                        
                        case "-":
                            this.valorDisplay = (this.numeroAtual - displayAtual).toString();
                            break;
                        case "*":
                            this.valorDisplay = (this.numeroAtual * displayAtual).toString();
                            break;
                        case "/":
                            this.valorDisplay = (this.numeroAtual / displayAtual).toString();
                            break;

                    }//Fim do switch
                    this.numeroAnterior = this.numeroAtual;
                    this.numeroAtual = null;

                    if(this.operador != "0"){
                        this.operador = null;
                    }
                    //acertar o numero de casas decimais quando o resultado for decimal
                }//Fim do if
                else{
                    this.numeroAnterior = displayAtual;
                }//fim do else
            }//Fim do if numeroAtual
            this.operador = operacao;
            this.numeroAtual = parseFloat(this.valorDisplay);
            if(this.operador != "="){
                this.valorDisplay = "0";
            }
        },//Fim operações
    
        login(){
            //simulando uma requisisão de login assincrona 
            setTimeout(() => {
                if((this.usuario === "mari" && this.senha === "123") || 
                (this.usuario === "otavio" && this.senha === "maltinha123") ||
                (this.usuario === "adriano" && this.senha === "adri123") ||
                (this.usuario === "julia" && this.senha === "ju123")){
                    this.erro = null;
                    this.sucesso = "Login efetuado com sucesso!💕";
                    window.location.href='index.calculadora.html';
                    // alert("Login efetuado com sucesso!");
                }//fechamento do if
                else{
                    //alert ("usuario ou senha incorretos!");
                    this.erro = "Usuário ou senha incorretos!💔";
                    this.sucesso = null;
                }
            }, 1000);
        },//fechamento login
    },//Fechamento methods
}).mount("#app");//Fechamento app